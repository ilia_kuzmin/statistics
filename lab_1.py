import numpy as np
import matplotlib.pyplot as plt
from distributions import Distribution, get_sample, get_pdf_fun


def get_pdf_data(distribution, sample):
    if distribution is Distribution.Poisson:
        x = np.arange(max(sample) + 1)
        return x, get_pdf_fun(distribution, x)
    x = np.linspace(min(sample), max(sample), 1000)
    return x, get_pdf_fun(distribution, x)


def draw_plot(distribution):
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey='row', figsize=(14, 6)) if \
        distribution is not Distribution.Cauchy else plt.subplots(1, 3, figsize=(14, 6))

    sample_10 = get_sample(distribution, 10)
    ax1.hist(sample_10, density=True)
    x, y = get_pdf_data(distribution, sample_10)
    ax1.plot(x, y)
    ax1.set(xlabel='x', ylabel='Density')
    ax1.set_title(distribution.name + ' 10')

    sample_50 = get_sample(distribution, 50)
    ax2.hist(sample_50, density=True)
    x, y = get_pdf_data(distribution, sample_50)
    ax2.plot(x, y)
    ax2.tick_params(axis='y', reset=True)
    ax2.set(xlabel='x', ylabel='Density')
    ax2.set_title(distribution.name + ' 50')

    sample_1000 = get_sample(distribution, 1000)
    ax3.hist(sample_1000, density=True, label='1000')
    x_1000, y_1000 = get_pdf_data(distribution, sample_1000)
    ax3.plot(x_1000, y_1000)
    ax3.tick_params(axis='y', reset=True)
    ax3.set(xlabel='x', ylabel='Density')
    ax3.set_title(distribution.name + ' 1000')
    if distribution is Distribution.Cauchy:
        ax3.set_ylim(0., 0.001)
    fig.savefig(distribution.name + '.png')


def draw_plots():
    draw_plot(Distribution.Normal)
    draw_plot(Distribution.Cauchy)
    draw_plot(Distribution.Laplace)
    draw_plot(Distribution.Poisson)
    draw_plot(Distribution.Uniform)
