import numpy as np
import matplotlib.pyplot as plt
from distributions import Distribution, get_sample
from scipy.stats import norm, cauchy, laplace, poisson, uniform
from math import sqrt
from collections import defaultdict


def get_upper_quartile(sorted_s):
    n = len(sorted_s)
    if n % 4 == 0:
        return sorted_s[n // 4 * 3 - 1]
    return sorted_s[int(np.floor(n * 3 / 4.))]


def get_lower_quartile(sorted_s):
    n = len(sorted_s)
    if n % 4 == 0:
        return sorted_s[n // 4 - 1]
    return sorted_s[int(np.floor(n / 4.))]


def draw_boxplots(distributions, sizes):
    for distribution in distributions:
        fig, axes = plt.subplots(1, len(sizes))
        for size, ax in zip(sizes, axes):
            sample = get_sample(distribution, size)
            ax.boxplot(sample)
            ax.set_title(distribution.name + ' ' + str(size))
        fig.savefig('boxplots/' + distribution.name + '.png')


def get_whiskers(lower, upper):
    iqr = upper - lower
    return lower - 1.5 * iqr, upper + 1.5 * iqr


def get_norm_outliers_p():
    lower_quartile, upper_quartile = norm.ppf(0.25), norm.ppf(0.75)
    lower_whisker, upper_whisker = get_whiskers(lower_quartile, upper_quartile)
    outliers_p = norm.cdf(lower_whisker) + 1 - norm.cdf(upper_whisker)
    return outliers_p


def get_cauchy_outliers_p():
    lower_quartile, upper_quartile = cauchy.ppf(0.25), cauchy.ppf(0.75)
    lower_whisker, upper_whisker = get_whiskers(lower_quartile, upper_quartile)
    outliers_p = cauchy.cdf(lower_whisker) + 1 - cauchy.cdf(upper_whisker)
    return outliers_p


def get_laplace_outliers_p():
    lower_quartile, upper_quartile = laplace.ppf(0.25, scale=1. / sqrt(2.)), laplace.ppf(0.75, scale=1. / sqrt(2.))
    lower_whisker, upper_whisker = get_whiskers(lower_quartile, upper_quartile)
    outliers_p = laplace.cdf(lower_whisker, scale=1. / sqrt(2.)) + 1 - laplace.cdf(upper_whisker, scale=1. / sqrt(2.))
    return outliers_p


def get_poisson_outliers_p():
    lower_quartile, upper_quartile = poisson.ppf(0.25, mu=10), poisson.ppf(0.75, mu=10)
    lower_whisker, upper_whisker = get_whiskers(lower_quartile, upper_quartile)
    outliers_p = poisson.cdf(lower_whisker, mu=10) + 1 - poisson.cdf(upper_whisker, mu=10) - \
                 poisson.pmf(lower_whisker, mu=10)
    return outliers_p


def get_uniform_outliers_p():
    lower_quartile, upper_quartile = uniform.ppf(0.25, loc=-sqrt(3.), scale=2. * sqrt(3.)), \
                                     uniform.ppf(0.75, loc=-sqrt(3.), scale=2. * sqrt(3.))
    lower_whisker, upper_whisker = get_whiskers(lower_quartile, upper_quartile)
    outliers_p = uniform.cdf(lower_whisker, loc=-sqrt(3.), scale=2. * sqrt(3.)) + 1 - \
                 uniform.cdf(upper_whisker, loc=-sqrt(3.), scale=2. * sqrt(3.))
    return outliers_p


def print_theoretical_outliers(distributions):
    outliers = [get_norm_outliers_p(), get_cauchy_outliers_p(), get_laplace_outliers_p(),
                get_poisson_outliers_p(), get_uniform_outliers_p()]

    with open('theory_table.tex', 'w', encoding='utf8') as file:
        file.write('\\begin{tabular}{|c|c|}\n')
        file.write('\\hline\n')
        file.write('Распределение & Доля выбросов \\\\ \n')
        file.write('\\hline \\hline \n')
        for distribution, outlier in zip(distributions, outliers):
            file.write(distribution.name + ' & {:.3f}'.format(np.around(outlier, 3)) + '\\\\ \n')
            file.write('\\hline \n')
        file.write('\\end{tabular}\n')
        file.write('\\caption{Теоретическая доля выбросов}\n')


def calculate_experiment_outliers(distributions, sizes):
    outliers = defaultdict(lambda: defaultdict(float))
    for distribution in distributions:
        dist_outliers = defaultdict(float)
        for size in sizes:
            for _ in range(1000):
                sample = get_sample(distribution, size)
                sorted_s = np.sort(sample)
                lower_quartile, upper_quartile = get_lower_quartile(sorted_s), get_upper_quartile(sorted_s)
                lower_whisker, upper_whisker = get_whiskers(lower_quartile, upper_quartile)
                out_percent = (len(sorted_s[sorted_s < lower_whisker]) + len(sorted_s[sorted_s > upper_whisker])) \
                              / len(sorted_s)
                dist_outliers[size] += out_percent
        for size in sizes:
            outliers[distribution][size] = dist_outliers[size] / 1000
    return outliers


def print_experiment_outliers(distributions, sizes, outliers):
    with open('experiment_table.tex', 'w', encoding='utf8') as file:
        file.write('\\begin{tabular}{|c|c|}\n')
        file.write('\\hline\n')
        file.write('Выборка & Доля выбросов \\\\ \n')
        file.write('\\hline \\hline \n')
        for distribution in distributions:
            for size in sizes:
                file.write(distribution.name + ' ' + str(size) +
                           ' & {:.2f}'.format(np.around(outliers[distribution][size], 2)) + '\\\\ \n')
                file.write('\\hline \n')
        file.write('\\end{tabular}\n')
        file.write('\\caption{Экспериментальная доля выбросов}\n')


def lab_3():
    distributions = [Distribution.Normal, Distribution.Cauchy, Distribution.Laplace,
                     Distribution.Poisson, Distribution.Uniform]
    sizes = [20, 100]

    # draw_boxplots(distributions, sizes)
    print_theoretical_outliers(distributions)
    outliers = calculate_experiment_outliers(distributions, sizes)
    print_experiment_outliers(distributions, sizes, outliers)
