import numpy as np
import matplotlib.pyplot as plt
from distributions import Distribution, get_sample, get_cdf, get_pdf_fun
from scipy import stats


def get_ecdf(sample, x):
    return len(sample[sample < x]) / len(sample)


def draw_ecdf(distributions, sizes):
    for distribution in distributions:
        fig, axes = plt.subplots(1, len(sizes), figsize=(12, 5))
        for size, ax in zip(sizes, axes):
            sample = get_sample(distribution, size)
            x = np.linspace(6, 14, 1000) if distribution is Distribution.Poisson else np.linspace(-4, 4, 1000)
            ecdf = np.array(list(map(lambda z: get_ecdf(sample, z), x)))
            ax.plot(x, ecdf, c='g', label='ecdf')
            cdf = get_cdf(distribution, x)
            ax.plot(x, cdf, c='r', label='cdf')
            ax.set(xlabel='x', ylabel='F(x)')
            ax.set_title(distribution.name + ' ' + str(size))
        plt.legend(bbox_to_anchor=(1.04, 0.5), loc="center left", borderaxespad=0)
        fig.savefig('cdf/' + distribution.name + '.png')


def get_title_by_factor(factor):
    eps = 1e-14
    if abs(factor - 0.5) < eps:
        return '$\\frac{h^S_n}{2}$'
    if abs(factor - 1.) < eps:
        return '$h^S_n$'
    if abs(factor - 2.) < eps:
        return '$2h^S_n$'


def kernel(x):
    return 1./np.sqrt(2. * np.pi) * np.exp(-x**2 / 2.)


def get_kde(sample, h, x):
    return 1./(len(sample) * h) * np.sum([kernel((x - x_i) / h) for x_i in sample])


def draw_kde(distributions, sizes, factors):
    for distribution in distributions:
        for size in sizes:
            sample = get_sample(distribution, size)
            fig, axes = plt.subplots(1, len(factors), figsize=(14, 5))
            x = np.linspace(6, 14, 1000) if distribution is Distribution.Poisson else np.linspace(-4, 4, 1000)
            left, right = (6, 14) if distribution is Distribution.Poisson else (-4, 4)
            pdf = get_pdf_fun(distribution, x)
            for factor, ax in zip(factors, axes):
                trimmed = sample[sample <= right]
                trimmed = trimmed[left <= trimmed]
                h_s = np.power(4. / 3., 1. / 5.) * np.std(trimmed) * np.power(len(trimmed), -1./5.)
                kde = np.array([get_kde(trimmed, h_s * factor, elem) for elem in x])
                ax.plot(x, pdf, c='g', label='pdf')
                ax.plot(x, kde, c='r', label='kde')
                ax.set_title(get_title_by_factor(factor))
                ax.set(xlabel='x', ylabel='f(x)')
            fig.suptitle(distribution.name + ' ' + str(size))
            plt.legend(bbox_to_anchor=(1.04, 0.5), loc="center left", borderaxespad=0)
            fig.savefig('kde/' + distribution.name + str(size) + '.png')


def lab_4():
    distributions = [Distribution.Normal, Distribution.Cauchy, Distribution.Laplace,
                     Distribution.Poisson, Distribution.Uniform]
    distributions = [Distribution.Normal]
    sizes = [20, 60, 100]
    factors = [0.5, 1., 2.]
    # draw_ecdf(distributions, sizes)
    draw_kde(distributions, sizes, factors)
