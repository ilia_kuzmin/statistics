import numpy as np
from scipy.stats import norm, cauchy, laplace, poisson, uniform
from enum import Enum
from math import sqrt
from scipy.special import factorial


class Distribution(Enum):
    Normal = 1
    Cauchy = 2
    Laplace = 3
    Poisson = 4
    Uniform = 5


def get_sample(distribution, size):
    if distribution is Distribution.Normal:
        return np.random.standard_normal(size=size)
    if distribution is Distribution.Cauchy:
        return np.random.standard_cauchy(size=size)
    if distribution is Distribution.Laplace:
        return np.random.laplace(loc=0., scale=1. / sqrt(2.), size=size)
    if distribution is Distribution.Poisson:
        return np.random.poisson(lam=10., size=size)
    if distribution is Distribution.Uniform:
        return np.random.uniform(low=-sqrt(3.), high=sqrt(3.), size=size)


def get_pdf_fun(distribution, x):
    if distribution is Distribution.Normal:
        return norm.pdf(x)
    if distribution is Distribution.Cauchy:
        return cauchy.pdf(x)
    if distribution is Distribution.Laplace:
        return laplace.pdf(x, scale=1. / sqrt(2.))
    if distribution is Distribution.Poisson:
        return np.exp(-10.) * (np.power(10., x) / factorial(x))
    if distribution is Distribution.Uniform:
        return uniform.pdf(x, loc=-sqrt(3.), scale=2. * sqrt(3.))


def get_cdf(distribution, x):
    if distribution is Distribution.Normal:
        return norm.cdf(x)
    if distribution is Distribution.Cauchy:
        return cauchy.cdf(x)
    if distribution is Distribution.Laplace:
        return laplace.cdf(x, scale=1. / sqrt(2.))
    if distribution is Distribution.Poisson:
        return poisson.cdf(x, mu=10.)
    if distribution is Distribution.Uniform:
        return uniform.cdf(x, loc=-sqrt(3.), scale=2. * sqrt(3.))
