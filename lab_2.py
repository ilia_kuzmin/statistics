from distributions import Distribution, get_sample
from scipy.stats import trim_mean
import copy
import numpy as np


def mid_quartile(sample):
    sorted_s = np.sort(sample)
    n = len(sample)
    if n % 4 == 0:
        upper = sorted_s[n // 4 * 3 - 1]
        lower = sorted_s[n // 4 - 1]
    else:
        upper = sorted_s[int(np.floor(n * 3 / 4.))]
        lower = sorted_s[int(np.floor(n / 4.))]

    return (upper + lower) / 2


def mid_extremal(sample):
    return (np.min(sample) + np.max(sample)) / 2


def get_rus_caption(distribution):
    if distribution is Distribution.Normal:
        return 'Нормальное распределение'
    if distribution is Distribution.Cauchy:
        return 'Распределение Коши'
    if distribution is Distribution.Laplace:
        return 'Распределение Лапласа'
    if distribution is Distribution.Poisson:
        return 'Распределение Пуассона'
    if distribution is Distribution.Uniform:
        return 'Равномерное распределение'


def get_data_row(data, _type):
    return "{:.5f} & {:.5f} & {:.5f} & {:.5f} & {:.5f}".format(data['mean'][_type], data['med'][_type],
                                                               data['z_r'][_type], data['z_q'][_type],
                                                               data['z_tr'][_type])


def create_table(distribution, sizes, data):
    with open('tables/' + distribution.name + '.tex', 'w', encoding='utf8') as file:
        file.write('\\begin{tabular}{|c|||c|c|c|c|c|}\n')
        file.write('\\hline\n')
        file.write(distribution.name + ' & $\\overline{x}$ & $med \\, x$ & $z_R$ & $z_Q$ & $z_{tr}$ \\\\ \n')
        file.write('\\hline \\hline \\hline \n')
        for size in sizes:
            file.write('$E(z)$ ' + str(size) + ' & ' + get_data_row(data[size], 'mean') + '\\\\ \n')
            file.write('\\hline\n')
            file.write('$D(z)$ ' + str(size) + ' & ' + get_data_row(data[size], 'var') + '\\\\ \n')
            if not size == sizes[-1]:
                file.write('\\hline')
            file.write('\\hline\n')
        file.write('\\end{tabular}\n')
        file.write('\\caption{' + get_rus_caption(distribution) + '}\n')


def create_tables():
    distributions = [Distribution.Normal, Distribution.Cauchy, Distribution.Laplace,
                     Distribution.Poisson, Distribution.Uniform]
    sizes = [10, 100, 1000]
    for distribution in distributions:
        data = {}
        for size in sizes:
            mean_v, med_v, mid_ext_v, mid_quart_v, trim_v = [], [], [], [], []
            for _ in range(1000):
                sample = get_sample(distribution, size)
                mean_v.append(np.mean(sample))
                med_v.append(np.median(sample))
                mid_ext_v.append(mid_extremal(sample))
                mid_quart_v.append(mid_quartile(sample))
                trim_v.append(trim_mean(sample, 0.25))
            data_s = {'mean': {'mean': np.around(np.mean(mean_v), 5), 'var': np.around(np.var(mean_v), 5)},
                      'med': {'mean': np.around(np.mean(med_v), 5), 'var': np.around(np.var(med_v), 5)},
                      'z_r': {'mean': np.around(np.mean(mid_ext_v), 5), 'var': np.around(np.var(mid_ext_v), 5)},
                      'z_q': {'mean': np.around(np.mean(mid_quart_v), 5), 'var': np.around(np.var(mid_quart_v), 5)},
                      'z_tr': {'mean': np.around(np.mean(trim_v), 5), 'var': np.around(np.var(trim_v), 5)}}
            data[size] = data_s
        create_table(distribution, sizes, data)
